resource "aws_s3_bucket_policy" "bucket_policy" {
  bucket = bucket-a

  policy = {
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "AWS": "arn:aws:iam::____________:user-a/srv_bucket-a"
      },
      "Action": [ "s3:*" ],
      "Resource": [
        "arn:aws:s3:::bucket-a",
        "arn:aws:s3:::bucket-a/*"
      ]
    }
  ]
}
}

resource "aws_s3_bucket_policy" "bucket_policy" {
  bucket = bucket-

  policy = {
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "AWS": "arn:aws:iam::____________:user-a/srv_bucket-b"
      },
      "Action":[
        "s3:ListBucket",
        "s3:GetObject"
      ],
      "Resource": [
        "arn:aws:s3:::bucket-b",
        "arn:aws:s3:::bucket-b/*"
      ]
    }
  ]
}
}
