import boto3
import os
import sys
import uuid
from urllib.parse import unquote_plus
from PIL import Image
import PIL.Image

#Creating Session With Boto3.
session = boto3.Session(
aws_access_key_id='Your Access Key ID',
aws_secret_access_key='You Secret access key'
)

#Creating S3 Resource From the Session.
s3 = session.resource('s3')

def remove_exif(src_path, target_path):
  with Image.open(image_path) as image:
      image.save(target_path)

def lambda_handler(event, context):
  for record in event['Records']:
      bucket = record['s3']['bucket']['name']
      file_key_name = unquote_plus(record['s3']['object']['key'])
      src_path = '/original/{}{}'.format(uuid.uuid4(), file_key_name)      
      touch_path = '/to_bucket_a/{}'.format(file_key_name)      
      s3.download_file(bucket, file_key_name, src_path)
      remove_exif(src_path, touch_path)
      s3.upload_file(touch_path, 'BUCKET_NAME_B', key)
