# gel

Step 1

A company allows their users to upload pictures to an S3 bucket. These pictures are always in the .jpg format.
The company wants these files to be stripped from any exif metadata before being shown on their website.
Pictures are uploaded to an S3 bucket A.
Create a system that retrieves .jpg files when they are uploaded to the S3 bucket A, removes any exif metadata,
and save them to another S3 bucket B. The path of the files should be the same in buckets A and B.

Step 2

To extend this further, we have two users User A and User B. Create IAM users with the following access:
- User A can Read/Write to Bucket A
- User B can Read from Bucket B


# Terraform execute

1. if first time to use terraform you need to run , it will help you to install the provider 
```bash
terraform init
```

2. check syntax of script before execute
```bash
terraform plan
```

3. execute script to create all resource
```bash
terraform apply
```
4. remove all resource
```bash
terraform destroy
```
5. remove target resource
```bash
terraform destroy -target <resource name>
```
